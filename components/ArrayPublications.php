<?php
$publications = [
    [
    'short' => 'The Poll',
    'description' => 'The poll is a simple but effective way to get your Fans to involve themselves in your brand. This post includes 2-4 options, and then asks Fans for their favorite. It’s also a great way to get some free market research!',
    'type' => 'post'
    ],
    [
        'short' => 'Ask for it',
        'description' => 'Sometimes the simplest, most straightforward idea is the best. Just like with Twitter, in which asking for a retweet increases the chance of it being done by 23 times, being clear about what you want can yield some great rewards.',
        'type' => 'post'
    ],
    [
        'short' => 'Give a sneak peek',
        'description' => 'Giving a sneak peek is one of the more complex Facebook posts, but has a more lasting draw.',
        'type' => 'post'
    ],
    [
        'title' => 'Allegory Examples',
        'text' => 'An allegory falls in line with the moral of a story. While an allegory is a story, poem, or picture, it\'s used to reveal a hidden meaning or message, like the moral. Allegories are exciting because they use characters and events to convey a meaning. They don\'t just come right out and say it. It is fun to sniff out the artist\'s intent and see what you can walk away with after you\'ve read a piece of writing or studied a piece of art. It\'s safe to say the creator\'s intent is always to inspire, whether that\'s to elicit an emotional response or get you to think about something in a new or different way.',
        'type' => 'article'
    ],
    [
        'title' => 'Analogy Examples',
        'text' => 'At its most basic, an analogy is a comparison of two things to show their similarities. Sometimes the things being compared are quite similar, but other times they could be very different. Nevertheless, an analogy explains one thing in terms of another to highlight the ways in which they are alike.',
        'type' => 'article'
    ],
    [
        'title' => 'Comma Splice Examples',
        'text' => 'A comma splice is a common grammatical error in English. Writers most often make this mistake when they are trying to "write by ear." It\'s a common idea that a comma indicates a pause where a reader or speaker should take a breath, but simply adding commas when you feel a break is needed is not a reliable way to make sure you\'re punctuating your sentences correctly.',
        'type' => 'article'
    ],
];