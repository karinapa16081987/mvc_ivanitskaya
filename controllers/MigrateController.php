<?php

require_once '../components/ArrayPublications.php';

class MigrateController extends Model
{

    public function actionIndex()
    {
        $sql = '
        CREATE TABLE if not exists posts (
            `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
            `short` VARCHAR(255),
            `description` TEXT
            )
        ';
        $this->db->exec($sql);

        $sql = '
        CREATE TABLE if not exists articles (
            `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
            `title` VARCHAR(255),
            `text` TEXT
            )
        ';
        $this->db->exec($sql);
        foreach ($publications as $publication){
            if ($publication['type'] == 'post'){

                $sql = '
            INSERT INTO posts SET 
                `short`=:short,
                `description`=:description
            ';
                $query = $this->db->prepare($sql);

                $query->bindValue(':short', $publication['short']);
                $query->bindValue(':description', $publication['description']);
                $query->execute();

            } elseif ($publication['type'] == 'article'){
                $sql = '
            INSERT INTO articles SET 
                `title`=:title,
                `text`=:text
            ';
                $query = $this->db->prepare($sql);

                $query->bindValue(':title', $publication['title']);
                $query->bindValue(':text', $publication['text']);

                $query->execute();
            }
        }
    }
}