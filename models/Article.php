<?php

class Article extends Model
{
    public static function readAllArticles()
    {
        try {
        $articles = [];
        $bd =new Model();

        $sql = 'SELECT * FROM articles ';

        $query = $bd->db->prepare($sql);

        $query->execute();

        $arrValues = $query->fetchAll();

        foreach ($arrValues as $key => $values){
            $articles[] =
                [
                'id' => $values['id'],
                'title' => $values['title'],
                'text' => $values['text']
                ];
        }
        }catch (PDOException $e){
            $e->getMessage();
        }
        return $articles;
    }

    public static function findOneArticle($id)
    {
        return self::readAllArticles()[$id];
    }
}
