<?php

class Post extends Model
{
    public static function readAllPosts()
    {
        try {
        $articles = [];
        $bd =new Model();

        $sql = 'SELECT * FROM posts';

        $query = $bd->db->prepare($sql);

        $query->execute();

        $arrValues = $query->fetchAll();

        foreach ($arrValues as $key => $values){
            $articles[] = [
                'id' => $values['id'],
                'short' => $values['short'],
                'description' => $values['description']
            ];
        }
        }catch (PDOException $e){
            $e->getMessage();
        }
        return $articles;
    }

    public static function findOnePost($id)
    {
        return self::readAllPosts()[$id];
    }
}
