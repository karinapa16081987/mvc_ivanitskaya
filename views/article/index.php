<h1>Article</h1>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Articles</th>
        <th scope="col">Title</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($articles as $id => $article) : ?>
        <tr>
            <th scope="row"><?= $article['id'] ?></th>
            <td><a href="/article/article/<?= $id ?>"><?= $article['title'] ?></a></td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>
