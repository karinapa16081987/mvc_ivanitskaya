<h1>Blog</h1>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Posts</th>
        <th scope="col">Title</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($posts as $id => $post) : ?>
        <tr>
            <th scope="row"><?= $post['id'] ?></th>
            <td><a href="/blog/post/<?= $id ?>"><?= $post['short'] ?></a></td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>
